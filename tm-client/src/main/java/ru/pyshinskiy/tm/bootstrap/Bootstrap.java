package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.command.AbstractCommand;
import ru.pyshinskiy.tm.service.SessionService;
import ru.pyshinskiy.tm.service.TerminalService;

import javax.enterprise.inject.Any;
import javax.enterprise.inject.Instance;
import javax.inject.Inject;
import java.util.Optional;

@NoArgsConstructor
public class Bootstrap {

    @Inject
    private ISessionEndpoint sessionEndpoint;

    @Inject
    private TerminalService terminalService;

    @Inject
    private SessionService sessionService;

    @Inject @Any
    private Instance<AbstractCommand> commands;

    public void start() throws Exception {
        System.out.println("***WELCOME TO TASK MANAGER***");
        @Nullable String command;
        while(true) {
            command = terminalService.nextLine();
            try {
                execute(command);
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }
    }

    private void execute(@Nullable final String command) throws Exception {
        if(command == null || command.isEmpty()) return;
        if("exit".equals(command)) {
            if(sessionService.getSessionDTO() != null) {
                sessionEndpoint.removeSession(sessionService.getSessionDTO().getUserId(), sessionService.getSessionDTO().getId());
            }
            System.exit(0);
        }
        @NotNull final Optional<AbstractCommand> optionalAbstractCommand = commands.stream().filter(e -> command.equals(e.command())).findFirst();
        @Nullable final AbstractCommand abstractCommand = optionalAbstractCommand.orElse(null);
        if(abstractCommand == null) {
            System.out.println("UNKNOW COMMAND");
            return;
        }
        if(!abstractCommand.isAllowed()) {
            System.out.println("!UNAVAILABLE COMMAND!");
            return;
        }
        abstractCommand.execute();
    }
}
