package ru.pyshinskiy.tm.command.task;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.TaskDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;
import java.util.List;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTask;
import static ru.pyshinskiy.tm.util.entity.EntityUtil.printTasks;

public final class TaskSelectAdminCommand extends AbstractCommand {

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "task_select_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "show any user selected task info";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[TASK SELECT]");
        System.out.println("ENTER TASK ID");
        @NotNull final List<TaskDTO> tasks = taskEndpoint.findAllTasks(sessionService.getSessionDTO());
        printTasks(tasks);
        final int taskNumber = Integer.parseInt(terminalService.nextLine()) - 1;
        @NotNull final String taskId = tasks.get(taskNumber).getId();
        @Nullable final TaskDTO task = taskEndpoint.findOneTask(sessionService.getSessionDTO(), taskId);
        if(task == null) {
            throw new Exception("task doesn't exist");
        }
        printTask(task);
        System.out.println("[OK]");
    }
}
