package ru.pyshinskiy.tm.command.project;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.Role;
import ru.pyshinskiy.tm.api.endpoint.SessionDTO;
import ru.pyshinskiy.tm.command.AbstractCommand;

import javax.inject.Inject;

import static ru.pyshinskiy.tm.util.entity.EntityUtil.printProjects;

public final class ProjectListAdminCommand extends AbstractCommand {

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Override
    public boolean isAllowed() {
        if(sessionService.getSessionDTO() == null) return false;
        return sessionService.getSessionDTO().getRole().equals(Role.ADMINISTRATOR);
    }

    @Override
    @NotNull
    public String command() {
        return "project_list_admin";
    }

    @Override
    @NotNull
    public String description() {
        return "list all users projects";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("[PROJECT LIST]");
        System.out.println("DO YOU WANT TO SORT PROJECTS?");
        @NotNull final String doSort = terminalService.nextLine();
        @NotNull final SessionDTO session = sessionService.getSessionDTO();
        if("y".equals(doSort) || "yes".equals(doSort)) {
            System.out.println("CHOOSE SORT TYPE");
            System.out.println("[createTime, startDate, finishDate, status]");
            @NotNull final String option = terminalService.nextLine();
            switch (option) {
                case "createTime" :
                    printProjects(projectEndpoint.sortProjectsByCreateTime(sessionService.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "startDate" :
                    printProjects(projectEndpoint.sortProjectsByStartDate(sessionService.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "finishDate" :
                    printProjects(projectEndpoint.sortProjectsByFinishDate(sessionService.getSessionDTO(), session.getUserId(), 1));
                    break;
                case "status" :
                    printProjects(projectEndpoint.sortProjectsByStatus(sessionService.getSessionDTO(), session.getUserId(), 1));
            }
            System.out.println("[OK]");
        }
        else {
            printProjects(projectEndpoint.findAllProjects(sessionService.getSessionDTO()));
            System.out.println("[OK]");
        }
    }
}
