package ru.pyshinskiy.tm.producer;

import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;
import ru.pyshinskiy.tm.endpoint.ProjectEndpointService;
import ru.pyshinskiy.tm.endpoint.SessionEndpointService;
import ru.pyshinskiy.tm.endpoint.TaskEndpointService;
import ru.pyshinskiy.tm.endpoint.UserEndpointService;

import javax.enterprise.inject.Produces;

public class EndpointProducer {

    @Produces
    public IProjectEndpoint projectEndpoint() {
        return new ProjectEndpointService().getProjectEndpointPort();
    }

    @Produces
    public ITaskEndpoint taskEndpoint() {
        return new TaskEndpointService().getTaskEndpointPort();
    }

    @Produces
    public IUserEndpoint userEndpoint() {
        return new UserEndpointService().getUserEndpointPort();
    }

    @Produces
    public ISessionEndpoint sessionEndpoint() {
        return new SessionEndpointService().getSessionEndpointPort();
    }
}
