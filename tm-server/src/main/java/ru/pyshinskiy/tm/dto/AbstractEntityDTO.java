package ru.pyshinskiy.tm.dto;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.io.Serializable;
import java.util.UUID;

@NoArgsConstructor
@Getter
@Setter
public abstract class AbstractEntityDTO implements Serializable {

    private static final long SerialVersionUID = 1L;

    @NotNull
    private String id = UUID.randomUUID().toString();
}
