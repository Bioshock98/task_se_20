package ru.pyshinskiy.tm.producer;

import org.apache.deltaspike.jpa.api.entitymanager.PersistenceUnitName;
import org.hibernate.boot.Metadata;
import org.hibernate.boot.MetadataSources;
import org.hibernate.boot.registry.StandardServiceRegistry;
import org.hibernate.boot.registry.StandardServiceRegistryBuilder;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.annotation.CustomEntityManagerFactory;
import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.entity.Session;
import ru.pyshinskiy.tm.entity.Task;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.service.property.PropertyService;

import javax.enterprise.context.ApplicationScoped;
import javax.enterprise.inject.Disposes;
import javax.enterprise.inject.Produces;
import javax.inject.Inject;
import javax.persistence.EntityManagerFactory;
import java.util.HashMap;
import java.util.Map;

public class EntityManagerFactoryProducer {

    @Inject
    private PropertyService propertyService;

    @Produces
    @ApplicationScoped
    @CustomEntityManagerFactory
    @PersistenceUnitName("ENTERPRISE")
    public EntityManagerFactory entityManagerFactory() {
        @NotNull final Map<String, String> settings = new HashMap<>();
        settings.put(org.hibernate.cfg.Environment.DRIVER, propertyService.getDriver());
        settings.put(org.hibernate.cfg.Environment.URL, propertyService.getHost());
        settings.put(org.hibernate.cfg.Environment.USER, propertyService.getLogin());
        settings.put(org.hibernate.cfg.Environment.PASS, propertyService.getPassword());
        settings.put(org.hibernate.cfg.Environment.DIALECT, "org.hibernate.dialect.MySQL5InnoDBDialect");
        settings.put(org.hibernate.cfg.Environment.HBM2DDL_AUTO, "update");
        settings.put(org.hibernate.cfg.Environment.SHOW_SQL, "true");
        @NotNull final StandardServiceRegistryBuilder registryBuilder = new StandardServiceRegistryBuilder();
        registryBuilder.applySettings(settings);
        registryBuilder.loadProperties("cache.properties");
        @NotNull final StandardServiceRegistry registry = registryBuilder.build();
        @NotNull final MetadataSources sources = new MetadataSources(registry);
        sources.addAnnotatedClass(Task.class);
        sources.addAnnotatedClass(Project.class);
        sources.addAnnotatedClass(User.class);
        sources.addAnnotatedClass(Session.class);
        @NotNull final Metadata metadata = sources.getMetadataBuilder().build();
        return metadata.getSessionFactoryBuilder().build();
    }

    public void close(@Disposes @CustomEntityManagerFactory EntityManagerFactory entityManagerFactory) {
        entityManagerFactory.close();
    }
}
