package ru.pyshinskiy.tm.producer;

import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.util.server.ServerInfo;

import javax.enterprise.inject.Produces;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class ServerInfoProducer {

    @Produces
    public ServerInfo serverInfo() throws UnknownHostException {
        @NotNull final ServerInfo serverInfo = new ServerInfo();
        serverInfo.setHost(InetAddress.getLocalHost().toString());
        serverInfo.setPort(Integer.parseInt(System.getProperty("server.port")));
        return serverInfo;
    }
}
