package ru.pyshinskiy.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CacheConcurrencyStrategy;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.dto.SessionDTO;
import ru.pyshinskiy.tm.enumerated.Role;

import javax.persistence.*;
import java.util.Date;

@Getter
@Setter
@Entity
@Cacheable
@NoArgsConstructor
@Table(name = "app_session")
@org.hibernate.annotations.Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Session extends AbstractEntity {

    @ManyToOne
    @NotNull
    private User user;

    @Basic(optional = false)
    @Enumerated(EnumType.STRING)
    @NotNull
    private Role role;

    @Basic(optional = false)
    @NotNull
    private Date timestamp = new Date(System.currentTimeMillis());

    @Basic
    @Nullable
    private String signature;

    @Nullable
    public static SessionDTO toSessionDTO(@Nullable final Session session) {
        if(session == null) return null;
        SessionDTO sessionDTO = new SessionDTO();
        sessionDTO.setId(session.getId());
        sessionDTO.setUserId(session.getUser().getId());
        sessionDTO.setTimestamp(session.getTimestamp());
        sessionDTO.setRole(session.getRole());
        return sessionDTO;
    }
}
