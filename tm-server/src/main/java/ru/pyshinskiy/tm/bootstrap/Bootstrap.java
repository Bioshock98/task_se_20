package ru.pyshinskiy.tm.bootstrap;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.pyshinskiy.tm.api.endpoint.IProjectEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ISessionEndpoint;
import ru.pyshinskiy.tm.api.endpoint.ITaskEndpoint;
import ru.pyshinskiy.tm.api.endpoint.IUserEndpoint;

import javax.inject.Inject;
import javax.persistence.criteria.CriteriaBuilder;
import javax.xml.ws.Endpoint;
import java.net.InetAddress;
import java.net.UnknownHostException;

@NoArgsConstructor
public class Bootstrap {

    @Inject
    private SessionCleaner sessionCleaner;

    @Inject
    private IProjectEndpoint projectEndpoint;

    @Inject
    private ITaskEndpoint taskEndpoint;

    @Inject
    private IUserEndpoint userEndpoint;

    @Inject
    private ISessionEndpoint sessionEndpoint;

    public void start() {
        @NotNull final String portNumber = System.getProperty("server.port");
        Endpoint.publish("http://0.0.0.0:" + portNumber + "/projectService?wsdl", projectEndpoint);
        Endpoint.publish("http://0.0.0.0:" + portNumber + "/taskService?wsdl", taskEndpoint);
        Endpoint.publish("http://0.0.0.0:" + portNumber + "/userService?wsdl", userEndpoint);
        Endpoint.publish("http://0.0.0.0:" + portNumber + "/sessionService?wsdl", sessionEndpoint);
        sessionCleaner.setDaemon(true);
        sessionCleaner.start();
    }
}
