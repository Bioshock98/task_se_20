package ru.pyshinskiy.tm;

import ru.pyshinskiy.tm.bootstrap.Bootstrap;

import javax.enterprise.inject.se.SeContainerInitializer;
import java.net.UnknownHostException;

public final class App {

    public static void main(String[] args) {
        SeContainerInitializer.newInstance()
                .addPackages(App.class).initialize()
                .select(Bootstrap.class).get().start();
    }
}
