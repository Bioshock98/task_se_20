package ru.pyshinskiy.tm.service.user;

import org.apache.deltaspike.jpa.api.transaction.Transactional;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.pyshinskiy.tm.entity.User;
import ru.pyshinskiy.tm.repository.IUserRepository;
import ru.pyshinskiy.tm.service.AbstractService;

import javax.inject.Inject;
import javax.persistence.NoResultException;
import java.util.List;

@Transactional
public class UserService extends AbstractService<User> implements IUserService {

    @Inject
    private IUserRepository userRepository;

    @Nullable
    @Override
    public User findOne(@Nullable final String id) throws Exception {
        if(id == null || id.isEmpty()) throw new Exception("user id is empty or null");
        @Nullable final User user = userRepository.findBy(id);
        return user;
    }

    @Override
    @NotNull
    public List<User> findAll() {
        @NotNull final List<User> users = userRepository.findAll();
        return users;
    }

    @Override
    public void persist(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        userRepository.save(user);
    }

    @Override
    public void merge(@Nullable final User user) throws Exception {
        if(user == null) throw new Exception("invalid user");
        userRepository.save(user);
    }

    @Override
    public void remove(@Nullable final String id) throws Exception {
        @Nullable final User user = findOne(id);
        if(user == null) return;
        userRepository.remove(user);
    }

    @Override
    public void removeAll() throws Exception {
        userRepository.removeAll();
    }

    @Override
    @Nullable
    public User getUserByLogin(@Nullable final String login) throws Exception {
        if(login == null) throw new Exception("login is invalid");
        @Nullable final User user;
        try {
            user = userRepository.getUserByLogin(login);
        }
        catch (NoResultException e) {
            return null;
        }
        return user;
    }
}
