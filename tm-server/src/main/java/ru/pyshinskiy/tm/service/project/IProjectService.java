package ru.pyshinskiy.tm.service.project;

import ru.pyshinskiy.tm.entity.Project;
import ru.pyshinskiy.tm.service.wbs.IAbstractVBSService;

public interface IProjectService extends IAbstractVBSService<Project> {
}
