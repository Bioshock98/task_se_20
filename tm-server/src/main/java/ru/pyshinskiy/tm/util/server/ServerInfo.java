package ru.pyshinskiy.tm.util.server;

import lombok.Getter;
import lombok.Setter;

import javax.enterprise.inject.Alternative;
import java.io.Serializable;

@Getter
@Setter
@Alternative
public class ServerInfo implements Serializable {

    private static final long SerialVersionUID = 1L;

    private String host;

    private int port;
}
